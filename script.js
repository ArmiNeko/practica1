/* 1- Escoger opcion correcta sobre los operadores || - &&
*/
var a= true
var b= false
var c= true

console.log((a&&b)||c); /* la respuesta correcta es la tercera opción, porque con || basta que uno sea verdadero para que todos sean verdaderon
*/

/* 2- Variables A=5, B=3, C= -12 . Indicar en cada caso si dara verdadero o falso
 
 1) true
 2) true
 3) false
 4) false
 5) true
 6) false
 7) true
 8) false
 9) true
 10) false
 11) true
 12) false 
 13) true
 14) true
 15) false
 */

 /* Describir y dar ejemplo de:
 1) git init   - es para agregar a una carpeta que ya a sido creada
 2) git clone  - sirve para clonar un proyecto con el URL 
 EJM: git clone + http://..... + la capeta donde lo quiereas clonar, si no existe la carpeta se creará una con el nombre que le des
 3) git push   - es para subir tus archivos 
 EJM: git push + origin master 
 4) git log    - lo utilizas para ver cuantos commits creaste
 EJM: si creaste 4 , 5 o más commits solo necesitas poner "git log" y te apareceran todos.
 5) git commit - sirve para agregar comentarios
 EJM: escribes git comit + " -m" seguido de comillas y entre estas colocas un comentario
 6) git status - es para comprobar que tus cambios se guardaron correctamente o si aund ebes de agregarlos.
 EJM: Si aun no pusiste git add .  entonces al colocar git status los archivos, carpetas que creaste saldran en color rojo
 7) git add    - sirve para agregar commits / carpetas 
 EJM: git add + " ." o tambien puedes ponerle un nombre de una vez
 8) git remote - sirve para ver que repositorios remotos especificaste
 9) git rm     - es para eliminar archivos
 EJM: colocar git rm seguido de loq ue deseas borrar
 10) git config- es para agregar tu correo electronico o tu nombre de usuario
 EJM: git config.name o git config.email seguido de tu correo o tu email  

*/